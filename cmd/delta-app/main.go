package main

import (
	"database/sql"
	"flag"
	"log"
	"os"

	"bitbucket.org/faisdelta/fais-delta/api"
	"bitbucket.org/faisdelta/fais-delta/web"

	_ "github.com/mattn/go-sqlite3"
)

var apiPort = flag.Int("apiport", 8081, "api port")
var webPort = flag.Int("webport", 8080, "web port")
var dbpath = flag.String("dbpath", "", "sqlite database path")

func main() {
	flag.Parse()
	if *dbpath == "" {
		flag.PrintDefaults()
		return
	}
	db, err := sql.Open("sqlite3", *dbpath)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()
	if err := db.Ping(); err != nil {
		log.Fatal(err)
	}
	go web.NewContext(*webPort,
		log.New(os.Stdout, "web: ", log.Lshortfile|log.Ldate|log.Ltime)).Start()
	api.NewContext(db, *apiPort,
		log.New(os.Stdout, "api: ", log.Lshortfile|log.Ldate|log.Ltime)).Start()
}
