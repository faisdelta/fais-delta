package web

import (
	"fmt"
	"log"
	"net/http"
)

type context struct {
	port   int
	logger *log.Logger
}

func NewContext(port int, logger *log.Logger) *context {
	return &context{
		port,
		logger,
	}
}

func (ctx *context) Start() {
	mux := http.NewServeMux()
	mux.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		http.Redirect(w, r, "/resources/", http.StatusFound)
	})
	mux.Handle("/resources/", http.StripPrefix("/resources/", http.FileServer(http.Dir("../../web/static"))))
	ctx.logger.Printf("starting on port : %d", ctx.port)
	ctx.logger.Fatal(http.ListenAndServe(fmt.Sprintf(":%d", ctx.port), mux))
}
