package api

import (
	"database/sql"
	"fmt"
	"log"
	"net/http"

	"github.com/naoina/denco"
)

type context struct {
	db     *sql.DB
	port   int
	logger *log.Logger
}

func NewContext(db *sql.DB, port int, logger *log.Logger) *context {
	return &context{
		db,
		port,
		logger,
	}
}

func (ctx *context) Start() {
	mux := denco.NewMux()
	handler, err := mux.Build([]denco.Handler{
		mux.GET("/", ctx.index),
	})
	if err != nil {
		panic(err)
	}
	ctx.logger.Printf("starting on port : %d", ctx.port)
	ctx.logger.Fatal(http.ListenAndServe(fmt.Sprintf(":%d", ctx.port), handler))
}

func (ctx *context) index(w http.ResponseWriter, r *http.Request, params denco.Params) {
	fmt.Fprintf(w, "Delta api")
}
